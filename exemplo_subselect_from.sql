select prof, inicio
from (
	select c.nome as curso, 
	   t.dt_inicio as inicio, p.nome as prof
	from cursos as c, turmas as t, professores as p
	where t.curso_id = c.id
	  and t.professor_id = p.id
) lista_de_turmas
where curso like '%java %'