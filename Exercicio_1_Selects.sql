# Os cursos de HTML, JAVA e C# Ordenados por Nome Decrescente.
select * from cursos
where nome like '%html%'
   or nome like '%java%'
   or nome like '%c#%'
order by nome desc;

# Os cursos onde há turmas atualmente.
# Com a cláusula IN
select nome
from cursos
where id in (
	select curso_id 
	from turmas
	where now() between dt_inicio 
 	  and dt_fim
);

# Com Join
select nome, 
       dt_inicio as 'Início', 
       dt_fim as Fim 
from turmas, cursos
where now() between dt_inicio 
  and dt_fim
  and curso_id = cursos.id;

# O curso da turma mais antiga.
# Com " = "
select nome
from cursos
where id = (
	select curso_id 
	from turmas
	order by dt_inicio
	limit 1
);

# Com Join
select nome 
from cursos as c, turmas as t
where t.curso_id = c.id
order by dt_inicio
limit 1

