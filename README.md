# Repositório da Turma MySQL Fundamentos 2019 Dezembro - Senai Informatica 1.32
## Bem Vindo
Aqui está disponibilizado o material do Curso MySQL Fundamentos, as apresentações, exercícios e respostas.
## Como obter
Para obter uma cópia deste conteúdo basta utilizar o comando:

Também é possível fazer o download através do link
[MySQL 2019 Dezembro](https://gitlab.com/JSWilProf/mysql-n1906)

# Ementa

## Programação do Curso (24h)

- Sistema gerenciador de banco de dados (SGBD)
- Tipos de Banco de Dados
- Os Bancos de dados mais utilizados
- Modelo Entidade e Relacionamento
- Análise de negócio e modelagem de dados
- Normalização de dados e redundância
- Chaves primárias e estrangeiras
- Inserção, Deleção e Atualização de dados
- União de dados
- Junção de dados
- Agrupamento e totalização de dados
- Ordenação de dados
