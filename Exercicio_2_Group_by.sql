# Resposta do exercicio 2 da apresentacao 6

# forma simples
select nome, count(professor_id) as qtd
from turmas, professores as p
where professor_id = p.id
group by professor_id
order by qtd desc
limit 1;

# com um subselect 
select nome as "nome do professor"
from (
	select professor_id as prof, count(professor_id) as qtd
	from turmas
	group by professor_id
	order by qtd desc
	limit 1
) total_cursos, professores
where prof = id;

# com dois subselect
select nome as "nome do professor"
from professores
where id = (
	select prof 
    from (
		select professor_id as prof, count(professor_id) as qtd
		from turmas
		group by professor_id
		order by qtd desc
		limit 1
	) total_cursos
)