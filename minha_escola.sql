-- MySQL dump 10.13  Distrib 8.0.18, for macos10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: minha_escola
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

create database minha_escola;
use minha_escola;

--
-- Table structure for table `alunos`
--

DROP TABLE IF EXISTS `alunos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alunos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `documento` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alunos`
--

LOCK TABLES `alunos` WRITE;
/*!40000 ALTER TABLE `alunos` DISABLE KEYS */;
INSERT INTO `alunos` VALUES (1,'Stevie Frizzell','8996201932113231301','Conrad_Frizzell61@example.com'),(2,'Jackeline Acker','8945002340102203243','jacker56@gmail.com'),(3,'Twila Bock','8999503113303202127','Schuler@example.com'),(4,'Reina Adkins','8937202324033203308','ZapataF@example.com'),(5,'Lavonia Whiteside','8923301023243311304','Sanford549@nowhere.com'),(6,'Kendrick Naranjo','8922001041022230223','knaranjo@gmail.com'),(7,'Kim Desantis','8955002344024242423','Barr@nowhere.com'),(8,'Raul Palma','8935301323210304325','Almanza@example.com'),(9,'Lorette Ortiz','8925401200334322343','Ron.Irish@example.com'),(10,'Jewel Plummer','8982002233233102415','ktercnnp_okcisx@example.com'),(11,'Carter Matlock','8962001033404314018','mnksgeiq8021@example.com'),(12,'Clemente Craddock','8999502311332201045','Atwood@nowhere.com'),(13,'Rolland Watson','8997201204140242002','rwatson34@gmail.com'),(14,'Kayleen Hogg','8935301313222131443','Sima_Call6@example.com'),(15,'Loise Mayers','8962001303341201344','Kirk.HKoehler@nowhere.com'),(16,'Marine Turley','8956002200334204319','EdmundBenitez631@example.com'),(17,'Debby Concepcion','8922003044312104200','Salley69@nowhere.com'),(18,'Kent Staton','8945002134211201949','Kiefer@nowhere.com'),(19,'Emmett Moulton','8910001441430202417','ElmerFoley@example.com'),(20,'Clarinda Acker','8942002000441310316','Brinkman28@example.com'),(21,'Darrick Cardoza','8957002010213104426','Almeida24@example.com'),(22,'Kecia Adcock','8935802304434442019','LambF@nowhere.com'),(23,'Maurice Mireles','8953001330233423243','ShandraCarranza@nowhere.com'),(24,'Ruben Simpson','8932002204342100123','JeraldAbraham529@example.com'),(25,'Britt Malley','8957002311111230314','bmalley928@yahoo.com.br'),(26,'Jamie Creech','8956003122311423017','jcreech@bol.com.br'),(27,'Luis Cruse','8957001313343032008','Abel5@example.com'),(28,'Carol Abrams','8922002112104032214','Bachman@example.com'),(29,'Adah Adair','8950101040332022133','FriedaAbreu99@example.com'),(30,'Henry Crump','8937101324202403247','Barrett_Mcgowan@example.com'),(31,'Magdalena Ogle','8955001332019241020','rvxkhcgk5666@example.com'),(32,'Donovan Allison','8953001323322132138','Dodge@example.com'),(33,'Jeremy Aranda','8953002201100003217','Perkins@example.com'),(34,'Camie Burch','8935203002320112115','NorrisBolt@example.com'),(35,'Robbie Canty','8936002202213403048','Barron@nowhere.com'),(36,'Lavone Amos','8935803423440434325','Duval@example.com'),(37,'Johnnie Allard','8959302112004143341','Zenobia.Brunson@example.com'),(38,'Debbra Acuna','8935302104224020442','Adela_Alley61@example.com'),(39,'Abdul Bergstrom','8925403301443132226','Abel313@example.com'),(40,'Nathanael Medina','8935402011224344114','Suggs@example.com'),(41,'Kasey Savoy','8935501223414201316','Acker@example.com'),(42,'Raphael Beckman','8961003133222201349','aweyzjpd3111@example.com'),(43,'Eugenia Johansen','8959303224041241301','Arthur@example.com'),(44,'Merissa Minter','8933002413320322016','Ruffin@nowhere.com'),(45,'Alejandro Norwood','8949002214042441040','anorwood123@uol.com.br'),(46,'Enoch Dortch','8926702343114434128','EleanoreYEscamilla158@example.com'),(47,'Kathleen Burnside','8925402440443002203','JerroldSAkins@example.com'),(48,'Stevie Vallejo','8962003420224112231','Tackett@example.com'),(49,'Gilda Abney','8955003321301012100','Chasidy.Nye82@example.com'),(50,'Pearlene Starnes','8961001130103403322','wawwcqng.wmbvidy@example.com');
/*!40000 ALTER TABLE `alunos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cursos`
--

DROP TABLE IF EXISTS `cursos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cursos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cursos`
--

LOCK TABLES `cursos` WRITE;
/*!40000 ALTER TABLE `cursos` DISABLE KEYS */;
INSERT INTO `cursos` VALUES (1,'BANCO DE DADOS MYSQL - FUNDAMENTOS'),(2,'CISCO CCNA R&S (200-125)'),(3,'COREL DRAW'),(4,'COREL DRAW - TÉCNICAS, EFEITOS E IMPRESSOS'),(5,'DESENVOLVIMENTO WEB - HTML5, CSS3 E JAVASCRIPT'),(6,'EXCEL - DASHBOARD E PLANILHAS INTERATIVAS'),(7,'EXCEL - MACROS E VBA'),(8,'EXCEL - MÓDULO I'),(9,'EXCEL - MÓDULO II'),(10,'ILLUSTRATOR'),(11,'INTRODUÇÃO Á INFORMÁTICA + OFFICE 2016'),(12,'PHOTOSHOP'),(13,'PHOTOSHOP - FERRAMENTAS PARA O MARKETING E MíDIAS SOCIAIS'),(14,'PHOTOSHOP - TÉCNICAS PROFISSIONAIS E TRATAMENTO DE IMAGEM'),(15,'PROGRAMAÇÃO C# - DESENVOLVIMENTO DE APLICAÇÕES WEB'),(16,'PROGRAMAÇÃO C# - FUNDAMENTOS'),(17,'PROGRAMAÇÃO JAVA - DESENVOLVIMENTO DE APLICAÇÕES WEB'),(18,'PROGRAMAÇÃO JAVA - DESENVOLVIMENTO DE APLICATIVOS ANDROID'),(19,'PROGRAMAÇÃO JAVA - FUNDAMENTOS'),(20,'PROGRAMAÇÃO JAVASCRIPT - DESENVOLVIMENTO DE APLICAÇÕES WEB'),(21,'PROGRAMAÇÃO JAVASCRIPT - DESENVOLVIMENTO DE APLICATIVOS MÓVEIS'),(22,'DESENVOLVIMENTO DE APLICATIVOS MÓVEIS');
/*!40000 ALTER TABLE `cursos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professores`
--

DROP TABLE IF EXISTS `professores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `professores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `documento` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professores`
--

LOCK TABLES `professores` WRITE;
/*!40000 ALTER TABLE `professores` DISABLE KEYS */;
INSERT INTO `professores` VALUES (1,'Norris Calderon','8945002104224443018','NorrisCalderon@example.com'),(2,'Aileen Pitts','8935202300424023412','TaggartY@example.com'),(3,'Alaine Hickey','8996402333204023026','JoeannPHuang@example.com'),(4,'Trey Christopher','8937101022212104025','Ada_Anglin6@nowhere.com'),(5,'Monroe Gatewood','8950201311043232032','LoydBranham258@example.com'),(6,'Adolfo Parks','8996101143424100142','oytxgpgm.ebekzhq@example.com'),(7,'Waltraud Lerma','8935302203242110427','SamuelGunn@example.com'),(8,'Clair Montes','8937201020323211133','Burgos@nowhere.com'),(9,'Lyndsay Biggs','8937201400141000416','Moreno@nowhere.com'),(10,'Candice Houser','8935702204200130003','GerryBarrow@example.com'),(11,'Buena Abreu','8942002042432131138','tsredn9393@example.com'),(12,'Kourtney Flood','8949003412411004046','Iliana.Acevedo52@example.com'),(13,'Minta Caruso','8945002022101423300','FlorencioF_Marlow7@nowhere.com'),(14,'Hailey Cardwell','8935202200424440121','Enoch_Tillery@nowhere.com'),(15,'Bonita Draper','8988002142141011325','MorrisAbraham@example.com'),(16,'Tawna Abel','8938501430043014236','DinoJaeger742@example.com'),(17,'Carylon Mintz','8933001220022000338','Rigby@example.com'),(18,'Gale Maldonado','8960003021423120443','Houser@example.com'),(19,'Alana Means','8959201112314044219','AdanHuerta523@example.com'),(20,'Allan Whittaker','8910001033222134431','CharlieNoriega688@example.com'),(21,'Verlie Burnside','8957001010010101307','vburnside@gmail.com'),(22,'Victor Tejeda','8926502422331104314','Abrams@example.com'),(23,'Addie Jacobsen','8956003002023231144','fazsmgzr.vbbx@example.com'),(24,'Andrew Quinonez','8962001411042233214','Cota424@example.com'),(25,'Luanne Jenkins','8939002444331011130','kqavbr3100@example.com'),(26,'Scotty Kinney','8935303241222104118','Brant.Abreu@example.com'),(27,'Venice Steffen','8950101114322443210','sneygl8904@example.com'),(28,'Tanna Moreno','8922002443010334221','stnhpti6@nowhere.com'),(29,'Felicidad Lentz','8959302442243002302','Fortner@nowhere.com'),(30,'Adalberto Bishop','8937203031220133400','Mccue@example.com'),(31,'Cristy Andre','8999501303224342340','LeighP_Redding7@example.com'),(32,'Josef Abrams','8925403201931014106','Montoya@nowhere.com'),(33,'Scarlett Dugger','8996201001130433200','Abney@nowhere.com'),(34,'Lala Bachman','8957001432244040411','Zack_Mcnair199@example.com'),(35,'Duane Alaniz','8957003411002320213','mlocd761@example.com'),(36,'Shirlee Healy','8942002013044100326','Slone@nowhere.com'),(37,'Monroe Hamblin','8942001002301233338','Mac_Amaya79@nowhere.com'),(38,'Velva Harder','8933001130100143008','Roush@example.com'),(39,'Kathie Bunn','8923302414102201228','Acosta@example.com'),(40,'Latosha Covey','8933001433031341347','Williford893@example.com'),(41,'Tommye Allman','8956001241004300416','AbernathyF@nowhere.com'),(42,'Florentino Cranford','8949003402413134341','Baugh@example.com'),(43,'Emilio Burnside','8959301004443022115','Sharpe26@example.com'),(44,'Winford Milner','8981002100221402003','wmilner@yahoo.com'),(45,'Andres Soria','8922001424131201929','MichalEdmond@example.com'),(46,'Gale Heffner','8997303111311100224','Perryman455@nowhere.com'),(47,'Myles Gatewood','8945003124411311443','vbzbfpb0034@nowhere.com'),(48,'Adalberto Thurston','8950101314313201028','Aguilera912@nowhere.com'),(49,'Maddie Abraham','8955001314024001300','mabraham22@gmail.com'),(50,'Barry Allred','8962001021334243133','Forrest718@nowhere.com');
/*!40000 ALTER TABLE `professores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turmas`
--

DROP TABLE IF EXISTS `turmas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `turmas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dt_inicio` date NOT NULL,
  `dt_fim` date NOT NULL,
  `duracao` time NOT NULL,
  `curso_id` int(11) NOT NULL,
  `professor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_turmas_cursos1_idx` (`curso_id`),
  KEY `fk_turmas_professores1_idx` (`professor_id`),
  CONSTRAINT `fk_turmas_cursos1` FOREIGN KEY (`curso_id`) REFERENCES `cursos` (`id`),
  CONSTRAINT `fk_turmas_professores1` FOREIGN KEY (`professor_id`) REFERENCES `professores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turmas`
--

LOCK TABLES `turmas` WRITE;
/*!40000 ALTER TABLE `turmas` DISABLE KEYS */;
INSERT INTO `turmas` VALUES (1,'2020-01-07','2020-02-04','17:45:21',5,33),(2,'2019-11-05','2019-11-28','13:45:10',11,37),(3,'2020-01-07','2020-02-15','11:08:42',2,41),(4,'2019-11-19','2020-01-19','10:01:07',2,39),(5,'2019-10-28','2019-11-12','14:35:25',2,38),(6,'2019-11-24','2020-01-23','19:48:40',9,33),(7,'2019-12-21','2020-02-17','19:42:28',18,4),(8,'2019-10-28','2019-11-10','15:00:16',18,15),(9,'2020-02-07','2020-04-04','11:37:25',11,42),(10,'2019-11-26','2020-01-15','15:27:47',13,41),(11,'2020-01-12','2020-02-28','14:35:25',18,31),(12,'2019-10-28','2019-12-04','10:10:12',12,23),(13,'2019-12-01','2019-12-10','16:15:06',1,6),(14,'2020-01-03','2020-02-10','14:27:17',4,29),(15,'2020-02-12','2020-03-18','17:10:40',10,22),(16,'2019-12-22','2019-12-26','10:00:00',15,17),(17,'2020-01-20','2020-03-03','15:14:58',15,49),(18,'2019-12-06','2019-12-12','13:10:58',10,16),(19,'2019-10-18','2019-11-15','19:16:21',7,25),(20,'2020-02-28','2020-04-08','12:12:47',18,47),(21,'2019-10-09','2019-10-23','19:34:13',3,1),(22,'2019-11-28','2020-01-20','12:17:11',1,7),(23,'2019-12-23','2020-01-06','10:00:08',7,32),(24,'2019-11-10','2019-12-10','11:04:18',12,21),(25,'2019-12-06','2020-01-09','13:02:11',14,45),(26,'2019-10-03','2019-10-08','10:16:42',11,30),(27,'2020-01-28','2020-02-14','10:01:04',12,49),(28,'2019-11-07','2019-12-28','10:00:02',10,31),(29,'2019-12-03','2020-01-13','11:45:09',5,32),(30,'2020-01-24','2020-02-19','17:57:05',12,37),(31,'2019-12-05','2020-01-16','10:01:37',14,32),(32,'2019-11-19','2019-12-17','16:42:20',13,19),(33,'2019-12-05','2020-01-18','10:10:54',11,38),(34,'2020-02-06','2020-02-12','16:53:52',22,31),(35,'2019-11-27','2019-12-10','12:40:17',8,25),(36,'2019-12-14','2019-12-25','18:00:56',6,42),(37,'2019-12-03','2019-12-07','14:53:24',1,4),(38,'2020-02-25','2020-04-13','11:57:03',1,37),(39,'2019-10-28','2019-12-18','11:05:29',17,24),(40,'2019-10-09','2019-11-27','10:36:43',5,43),(41,'2019-12-27','2020-02-25','13:41:34',15,44),(42,'2019-11-08','2019-12-19','10:00:06',15,44),(43,'2019-11-13','2019-12-09','12:03:36',10,25),(44,'2019-11-14','2019-12-29','10:09:34',3,50),(45,'2020-01-16','2020-02-07','10:00:04',14,50),(46,'2019-11-07','2019-11-24','10:00:40',9,30),(47,'2019-12-28','2020-02-20','15:35:00',6,8),(48,'2020-01-19','2020-03-03','13:42:13',12,37),(49,'2019-10-20','2019-11-03','16:05:49',5,29),(50,'2020-02-25','2020-03-30','15:14:14',6,24);
/*!40000 ALTER TABLE `turmas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turmas_has_alunos`
--

DROP TABLE IF EXISTS `turmas_has_alunos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `turmas_has_alunos` (
  `aluno_id` int(11) NOT NULL,
  `turma_id` int(11) NOT NULL,
  PRIMARY KEY (`aluno_id`,`turma_id`),
  KEY `fk_alunos_has_turmas_turmas1_idx` (`turma_id`),
  KEY `fk_alunos_has_turmas_alunos1_idx` (`aluno_id`),
  CONSTRAINT `fk_alunos_has_turmas_alunos1` FOREIGN KEY (`aluno_id`) REFERENCES `alunos` (`id`),
  CONSTRAINT `fk_alunos_has_turmas_turmas1` FOREIGN KEY (`turma_id`) REFERENCES `turmas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turmas_has_alunos`
--

LOCK TABLES `turmas_has_alunos` WRITE;
/*!40000 ALTER TABLE `turmas_has_alunos` DISABLE KEYS */;
INSERT INTO `turmas_has_alunos` VALUES (1,1),(20,1),(2,2),(9,2),(3,3),(10,3),(4,4),(7,4),(22,4),(29,4),(45,4),(5,5),(6,6),(14,6),(2,7),(7,7),(38,7),(6,8),(8,8),(12,8),(9,9),(18,9),(10,10),(19,10),(20,10),(31,10),(10,11),(11,11),(49,11),(4,12),(12,12),(33,12),(38,12),(9,13),(13,13),(44,13),(47,13),(14,14),(26,14),(31,14),(34,14),(15,15),(36,15),(13,16),(16,16),(21,16),(40,16),(9,17),(16,17),(17,17),(30,17),(18,18),(24,18),(50,18),(19,19),(32,19),(48,19),(20,20),(41,20),(3,21),(21,21),(30,21),(22,22),(43,22),(46,22),(23,23),(39,23),(19,24),(24,24),(40,24),(25,25),(46,25),(7,26),(14,26),(25,26),(26,26),(29,26),(33,26),(15,27),(23,27),(27,27),(36,27),(6,28),(28,28),(29,28),(29,29),(32,29),(34,29),(40,29),(30,30),(35,30),(27,31),(31,31),(32,32),(33,32),(46,32),(3,33),(8,33),(33,33),(29,34),(34,34),(44,34),(4,35),(16,35),(35,35),(2,36),(23,36),(36,36),(25,37),(37,37),(32,38),(38,38),(37,39),(39,39),(22,40),(34,40),(40,40),(41,40),(22,41),(28,41),(41,41),(4,42),(24,42),(42,42),(50,42),(26,43),(43,43),(11,44),(44,44),(16,45),(42,45),(45,45),(13,46),(23,46),(46,46),(1,47),(25,47),(47,47),(15,48),(17,48),(48,48),(6,49),(9,49),(13,49),(49,49),(30,50),(50,50);
/*!40000 ALTER TABLE `turmas_has_alunos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'minha_escola'
--

--
-- Dumping routines for database 'minha_escola'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-08 21:03:51
