insert into setor (nome) values ('recursos humanos');
select @recursos_humanos := last_insert_id();
insert into setor (nome) values ('contabilidade');
select @contabilidade := last_insert_id();
insert into setor (nome) values ('compras');
select @compras := last_insert_id();
insert into setor (nome) values ('suprimentos'); 
select @suprimentos := last_insert_id();
insert into setor (nome) values ('vendas');
select @vendas := last_insert_id();

insert into cargo (nome, salario) values ('analista', 2500);
select @analista := last_insert_id();
insert into cargo (nome, salario) values ('contador', 4200);
select @contador := last_insert_id();
insert into cargo (nome, salario) values ('auxiliar', 1500);
select @auxiliar := last_insert_id();
insert into cargo (nome, salario) values ('vendedor', 2700);
select @vendedor := last_insert_id();

insert into funcionario (nome, id_cargo, id_setor) 
values ('Fulano de tal', @analista, @recursos_humanos),
	   ('Beltrano da Silva', @contador, @contabilidade),
       ('Ciclano de Souza', @vendedor, @vendas),
       ('Ana Maria Arantes', @analista, @suprimentos),
       ('Maria Almeida Carvalho', @analista, @vendas);
select @maria_almeida := last_insert_id();
       
insert into gerente (id_gerente, id_setor) 
values (@maria_almeida, @vendas);
